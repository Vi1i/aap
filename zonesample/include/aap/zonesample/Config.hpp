#ifndef AAP_AAP_ZONECLEAN_HPP
#define AAP_AAP_ZONECLEAN_HPP

#include <string>
#include <ostream>

namespace aap { namespace zonesample {
class Config {
public:
  typedef struct {
    std::string inpath;
    std::string outpath;
  } data;

  Config();
  Config(std::string filename);
  auto GetConfigData() -> aap::zonesample::Config::data;

private:
  std::string filename_;
  aap::zonesample::Config::data datum_;

  auto Init() -> void;
};
}}
#endif
