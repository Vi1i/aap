#include <aap/zonesample/Config.hpp>

#include <memory>
#include <cstdlib>
#include <zlib.h>
#include <iostream>
#include <vector>
#include <map>
#include <cstring>
#include <string>
#include <filesystem>
#include <chrono>
#include <regex>
#include <fstream>
#include <algorithm>
#include <random>

std::ostream& operator<<(std::ostream& os, const aap::zonesample::Config::data& datum) {
  return os << "{'inpath':'" << datum.inpath << "','outpath':'" << datum.outpath << "'}";
}

typedef struct {
  unsigned int lines_total;
  unsigned int lines_in_sample;
  std::filesystem::path file;
} file_info;

auto sample(std::ifstream &inf, file_info fi, aap::zonesample::Config::data datum) -> file_info {
  float sample_percentage = 0.1;
  std::string line;
  std::vector<std::string> data;
  std::vector<std::string> samples;

  while(std::getline(inf, line)) {
    data.push_back(line);
  }
  fi.lines_total = data.size();
  if(fi.lines_total > 100) {
    int sample_size = fi.lines_total * sample_percentage;
    std::sample(data.begin(), data.end(), std::back_inserter(samples), sample_size, std::mt19937{std::random_device{}()});
    fi.lines_in_sample = samples.size();
  } else {
    //write entire contents to file
    samples = data;
    fi.lines_in_sample = fi.lines_total;
  }

  std::filesystem::path outfile(datum.outpath + "/" + fi.file.stem().stem().string() + ".aap");
  std::ofstream ofs;
  ofs.open(outfile);
  for(const auto &e : samples) {
    ofs << e << std::endl;
  }
  ofs.close();

  return fi;
}

auto main(int argc, char * argv[]) -> int {
  std::shared_ptr<aap::zonesample::Config> conf;
  if(argc == 2) {
    conf = std::make_shared<aap::zonesample::Config>(argv[1]);
  } else {
    conf = std::make_shared<aap::zonesample::Config>();
  }
  aap::zonesample::Config::data datum = conf->GetConfigData();

  std::cout << datum << std::endl;
  std::vector<std::filesystem::path> files;
  for(const auto &entry : std::filesystem::directory_iterator(datum.inpath)) {
    if(entry.path().extension() == ".aap") {
      files.push_back(entry.path());
    }
  }

  if (!std::filesystem::is_directory(datum.outpath) || !std::filesystem::exists(datum.outpath)) {
    std::filesystem::create_directory(datum.outpath);
  }

  auto mainStart = std::chrono::system_clock::now();
  for(const auto &f : files) {
    std::cout << "Opening: " << f << std::endl;
    auto start = std::chrono::system_clock::now();
    std::ifstream inf;
    inf.open(f.string());
    if (!inf) {
      std::cerr << "std::ifstream.open() of '" << f.string() << "' failed: " << strerror(errno);
      exit(1);
    }

    file_info fi;
    fi.file = f;
    fi = sample(inf, fi, datum);
    inf.close();

    unsigned int bytes = 0;

    auto end = std::chrono::system_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    std::cout << "\tTime Taken: " << elapsed.count() << "ms" << std::endl;
    std::cout << "\tLine Count: " << fi.lines_total << std::endl;
    std::cout << "\tSample Count: " << fi.lines_in_sample << std::endl;
  }
  auto end = std::chrono::system_clock::now();
  auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - mainStart);
  std::cout << std::endl << "Total time: " << elapsed.count() << "ms" << std::endl;

  return EXIT_SUCCESS;
}
