#include <aap/zonesample/Config.hpp>

#include <string>
#include <sstream>
#include <fstream>

aap::zonesample::Config::Config() {
  this->Init();
}

aap::zonesample::Config::Config(std::string filename) {
  filename_ = filename;
  this->Init();
}

auto aap::zonesample::Config::Init() -> void {
  char * xdg_config_home = std::getenv("XDG_CONFIG_HOME");
  if(xdg_config_home == NULL) {
    char * bash_home = std::getenv("HOME");
    if(bash_home == NULL) {
      //TODO: (Vi1i) Actually add this piece
      throw std::runtime_error("Needs implementation of getpuid perhaps...");
    }else{
      datum_.inpath = static_cast<std::string>(bash_home) + "/zonefiles.clean";
      datum_.outpath = datum_.inpath + ".sample";
    }
  }else{
    datum_.inpath = static_cast<std::string>(xdg_config_home) + "/zonefiles.clean";
    datum_.outpath = datum_.inpath + ".sample";
  }
  std::ifstream inFile(filename_, std::ios::in);
  if(!inFile.is_open()) {
    return;
  }

  std::string inpathStr("inpath");
  std::string outpathStr("outpath");

  std::string line;
  while(std::getline(inFile, line)) {
    std::string varName;
    std::string varVal;
    char delimeter = ':';

    std::stringstream ss;
    ss.str(line);
    std::getline(ss, varName, delimeter);
    std::getline(ss, varVal, delimeter);

    if(varName == inpathStr) {
      datum_.inpath = varVal;
    }else if(varName == outpathStr) {
      datum_.outpath = varVal;
    } else {
      continue;
    }
  }
}

auto aap::zonesample::Config::GetConfigData() -> aap::zonesample::Config::data {
  return datum_;
}
