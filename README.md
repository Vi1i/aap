# AAP
DNS Attack Surface Analysis with Dr. Andrew Kalafut, kalafuta@gvsu.edu

### Requirements
* pthreads
* clang++
* cmake
* STDC++17
* STDC++
* STDC++FS

### Compilation
* Move into `$PROJECT_ROOT/build`
* Execute `cmake ..`

### Execution
* Please copy `$PROJECT_ROOT/aap.conf-example` to `$PROJECT_ROOT/aap.conf`
* Modiy the configuration file for your system needs
* Execute the binary `$PROJECT_ROOT/bin/aap $PROJECT_ROOT/aap.conf`
---
### Notes
* This can take a while depending on the zonefile size, thread count, and network bandwidth
* The configuration file currently does not support changing the Resource Record
