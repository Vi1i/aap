// Copyright 2018 vi1i.petal@gmail.com

#include <aap/Conf.hpp>
#include <aap/DNS.hpp>

#include <fstream>
#include <iostream>
#include <istream>
#include <memory>
#include <sstream>
#include <string>

namespace aap {
    std::shared_ptr<aap::Conf> ReadConfigurationFile(const std::string & confPath) {
        std::string thread_count;
        std::string record_resource;
        std::string save_path;
        std::string zonefile_path;
        std::string useEdns0;
        std::string edns0_size;
        std::string socket_timeout;
        std::string dnssec;

        thread_count = "thread_count";
        record_resource = "record_resource";
        save_path = "save_path";
        zonefile_path = "zonefiles_path";
        useEdns0 = "use_edns0";
        edns0_size = "edns0_size";
        socket_timeout = "socket_timeout";
        dnssec = "dnssec";

        aap::Conf configuration;

        std::ifstream inFile(confPath, std::ios::in);
        if(!inFile.is_open()) {
            exit(EXIT_FAILURE);
        }
        std::string line;
        while(std::getline(inFile, line)) {
            std::string varName;
            std::string varVal;
            char delimeter;

            delimeter = ':';

            std::stringstream ss;
            ss.str(line);
            std::getline(ss, varName, delimeter);
            std::getline(ss, varVal, delimeter);

            if(varName == thread_count) {
                configuration.thread_count = std::stoi(varVal);
            }else if(varName == record_resource) {
                configuration.rr = aap::DNS::GetResourceRecord(varVal);
            }else if(varName == save_path) {
                configuration.save_path = varVal;
            }else if(varName == zonefile_path) {
                configuration.zonefile_path = varVal;
            }else if(varName == edns0_size) {
                configuration.edns0Size = std::stoi(varVal);
            }else if(varName == socket_timeout) {
                configuration.socketTimeout = std::stoi(varVal);
            }else if(varName == dnssec) {
                configuration.dnssec = std::stoi(varVal);
            }else if(varName == useEdns0) {
                configuration.useEdns0 = std::stoi(varVal);
            }else{
                continue;
            }
        }

        return std::shared_ptr<aap::Conf>(new aap::Conf(configuration));
    }
} // namespace aap
