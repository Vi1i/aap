// Copyright 2018 vi1i.petal@gmail.com

#include <aap/Conf.hpp>
#include <aap/View.hpp>
#include <aap/Model.hpp>
#include <aap/Controller.hpp>
#include <aap/DNS.hpp>

#include <unistd.h>

#include <cstdlib>
#include <iostream>
#include <memory>

int main(int argc, char* argv[]) {
  if(argc != 2) {
    return EXIT_FAILURE;
  }

  std::shared_ptr<aap::Conf> conf(aap::ReadConfigurationFile(argv[1]));
  if(!conf) {
    return EXIT_FAILURE;
  }

  std::shared_ptr<aap::View> view;
  view = std::unique_ptr<aap::View>(new aap::View(conf));
  aap::Controller app = aap::Controller(conf);
  app.Add(view);
  app.Run();

  return EXIT_SUCCESS;
}
