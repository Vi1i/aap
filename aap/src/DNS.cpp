// Copyright 2018 vi1i.petal@gmail.com

#include <aap/DNS.hpp>
#include <aap/Model.hpp>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <cstdlib>
#include <sys/socket.h>
#include <netdb.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <cerrno>
#include <cstring>
#include <iostream>
#include <iomanip>
#include <sstream>

#include <algorithm>

#include <fstream>
#include <string>

namespace aap {
  namespace DNS {
    void Notify(void* context, int notification, std::string filename, std::string message) {
      static_cast<aap::Model*>(context)->Notify(notification, filename, message);
    }

    void ChangeToDnsNameFormat(unsigned char *dns, unsigned char *host) {
      int lock = 0;
      int i;
      unsigned char * temp = new unsigned char[std::strlen((char*)host) + 2];
      strcpy((char*)temp, (char*)host);
      host = temp;
      strcat((char*)host,".");

      for(i = 0 ; i < strlen((char*)host) ; i++) {
        if(host[i]=='.') {
          *dns++ = i - lock;
          for(; lock < i; lock++)
          {
            *dns++ = host[lock];
          }
          lock++;
        }
      }
      *dns++ = '\0';
    }

    std::string GetHostByName(void* context, int socket, unsigned char *host, int query_type, int edns0_size, char *ns_ip, bool use_edns0 ,bool dnssec) {
      unsigned char buf[65536];
      unsigned char * qname;
      int i;
      int s;

      struct sockaddr_in dest;

      struct DNS_HEADER * dns = NULL;
      struct QUESTION * qinfo = NULL;
      struct OPT_RECORD * opt_rec = NULL;

      dest.sin_family = AF_INET;
      dest.sin_port = htons(53);
      dest.sin_addr.s_addr = inet_addr(ns_ip);

      dns = (struct DNS_HEADER *)&buf;

      dns->id = (unsigned short) htons(getpid());
      dns->qr = 0;
      dns->opcode = 0;
      dns->aa = 0;
      dns->tc = 0;
      dns->rd = 0;
      dns->ra = 0;
      dns->z = 0;
      dns->ad = 0;
      dns->cd = 0;
      dns->rcode = 0;
      dns->q_count = htons(1);
      dns->ans_count = 0;
      dns->auth_count = 0;
      dns->add_count = htons(1);

      qname = (unsigned char*)&buf[sizeof(struct DNS_HEADER)];

      ChangeToDnsNameFormat(qname, host);

      qinfo = (struct QUESTION*)&buf[sizeof(struct DNS_HEADER) + (strlen((const char*)qname) + 1)];

      qinfo->qtype = htons(query_type);
      qinfo->qclass = htons(1);

      ssize_t ret_size;
      ssize_t sent_size;
      if(use_edns0) {
        opt_rec = (struct OPT_RECORD*)&buf[sizeof(struct QUESTION) + sizeof(struct DNS_HEADER) + (strlen((const char*)qname) + 1)];
        opt_rec->domain_name = 0;
        opt_rec->type = htons(41);
        opt_rec->_class = htons((unsigned short)edns0_size);
        if(dnssec) {
          opt_rec->ttl = (1<<23);
        } else {
          opt_rec->ttl = (0<<23);
        }
        opt_rec->rd_len = 0;

        sent_size = sizeof(struct DNS_HEADER) + (strlen((const char*)qname) + 1) + sizeof(struct QUESTION) + sizeof(struct OPT_RECORD);
        ret_size = sendto(socket, (char*)buf, sizeof(struct DNS_HEADER) + (strlen((const char*)qname) + 1) + sizeof(struct QUESTION) + sizeof(struct OPT_RECORD), 0, (struct sockaddr*)& dest, sizeof(dest));
      } else {
        sent_size = sizeof(struct DNS_HEADER) + (strlen((const char*)qname) + 1) + sizeof(struct QUESTION);
        ret_size = sendto(socket, (char*)buf, sizeof(struct DNS_HEADER) + (strlen((const char*)qname) + 1) + sizeof(struct QUESTION), 0, (struct sockaddr*)& dest, sizeof(dest));
      }

      if(ret_size < 0) {
        Notify(context, aap::Model::Notification::ERROR, "GATHER - SENDTO", strerror(errno));
        return "ERROR:1:SENDTO";
      }

      i = sizeof dest;
      ret_size = recvfrom(socket,(char*)buf, 65536, 0, (struct sockaddr*)&dest, (socklen_t*)&i);
      if(ret_size < 0) {
        Notify(context, aap::Model::Notification::ERROR, "GATHER - RECFROM", strerror(errno));
        return "ERROR:1:RECVFROM";
      }

      std::string output = aap::DNS::hexStr(buf, ret_size);
      std::string line = std::to_string(sent_size) + "," + std::to_string(ret_size) + "," + output;

      return line;
    }

    std::string GetHostByName(void* context, int socket, unsigned char *host, char *ns_ip) {
      unsigned char buf[65536];
      unsigned char *qname;
      unsigned char *reader;
      unsigned int i;
      int j;
      int s;

      struct sockaddr_in a;

      struct RES_RECORD answer;
      struct sockaddr_in dest;

      struct DNS_HEADER *dns = NULL;
      struct QUESTION *qinfo = NULL;

      dest.sin_family = AF_INET;
      dest.sin_port = htons(53);
      dest.sin_addr.s_addr = inet_addr(ns_ip);

      dns = (struct DNS_HEADER*)&buf;

      dns->id = (unsigned short) htons(getpid());
      dns->qr = 0;
      dns->opcode = 0;
      dns->aa = 0;
      dns->tc = 0;
      dns->rd = 1;
      dns->ra = 0;
      dns->z = 0;
      dns->ad = 0;
      dns->cd = 0;
      dns->rcode = 0;
      dns->q_count = htons(1);
      dns->ans_count = 0;
      dns->auth_count = 0;
      dns->add_count = 0;

      qname = (unsigned char*)&buf[sizeof(struct DNS_HEADER)];

      ChangeToDnsNameFormat(qname, host);
      qinfo =(struct QUESTION*)&buf[sizeof(struct DNS_HEADER) + (strlen((const char*)qname) + 1)];

      qinfo->qtype = htons(aap::DNS::RR::A);
      qinfo->qclass = htons(1);

      ssize_t ret_size = sendto(socket,(char*)buf,sizeof(struct DNS_HEADER) + (strlen((const char*)qname) + 1) + sizeof(struct QUESTION), 0, (struct sockaddr*)&dest, sizeof(dest));
      if(ret_size < 0) {
        Notify(context, aap::Model::Notification::ERROR, "RESOLVE - SENDTO", strerror(errno));
        return "ERROR:2:SENDTO";
      }

      i = sizeof dest;
      ret_size = recvfrom(socket, (char*)buf, 65536, 0, (struct sockaddr*)&dest, &i);
      if(ret_size < 0) {
        Notify(context, aap::Model::Notification::ERROR, "RESOLVE - RECFROM", std::string(std::strerror(errno)));
        return "ERROR:2:RECVFROM";
      }

      dns = (struct DNS_HEADER*) buf;


      reader = &buf[sizeof(struct DNS_HEADER) + (strlen((const char*)qname) + 1) + sizeof(struct QUESTION)];
      std::ostringstream ss;
      //Notify(context, aap::Model::Notification::DEBUG, "BUF", hexStr(buf, ret_size));
      //Notify(context, aap::Model::Notification::DEBUG, "READER", hexStr(reader, ret_size - offset));

      int stop;
      std::string ip("ERROR:NOFOUND");
      for(auto record = 0; record < ntohs(dns->ans_count); ++record) {
        ReadName(reader, buf, &stop);
        reader = reader + stop;

        answer.resource = (struct R_DATA*)(reader);
        reader = reader + sizeof(struct R_DATA);

        if(ntohs(answer.resource->type) == aap::DNS::RR::A) {
          answer.rdata = (unsigned char*)malloc(ntohs(answer.resource->data_len) + 1);
          for(auto chunk = 0; chunk < ntohs(answer.resource->data_len); ++chunk) {
            answer.rdata[chunk] = reader[chunk];
          }
          answer.rdata[ntohs(answer.resource->data_len)] = '\0';
          long *p;
          p = (long*)answer.rdata;
          a.sin_addr.s_addr = (*p); //working without ntohl
          ip = inet_ntoa(a.sin_addr);
          free(answer.rdata);
          answer.rdata = NULL;
          break;
        }
      }

      return ip;
    }

    void ReadName(unsigned char* reader, unsigned char* buffer, int* count) {
      unsigned int p = 0;
      unsigned int jumped = 0;
      unsigned int offset;

      *count = 1;

      while(*reader != 0) {
        if(*reader >= 192) {
          offset = (*reader) * 256 + *(reader + 1) - 49152;
          reader = buffer + offset - 1;
          jumped = 1;
        }

        reader = reader+1;

        if(jumped == 0) {
          *count = *count + 1;
        }
      }
      if(jumped == 1) {
        *count = *count + 1;
      }
    }

    struct HexCharStruct {
      unsigned char c;
      HexCharStruct(unsigned char _c) : c(_c) { }
    };

    inline std::ostream& operator<<(std::ostream& o, const HexCharStruct& hs) {
      return (o << std::setfill('0') << std::setw(2) << std::hex << (int)hs.c);
    }

    inline HexCharStruct hex(unsigned char _c) {
      return HexCharStruct(_c);
    }

    std::string hexStr(unsigned char *data, int len) {
      std::stringstream ss("");
      for(auto pos = 0; pos < len; ++pos) {
        ss << hex(data[pos]);
      }

      return ss.str();
    }

    int GetSocket() {
      return socket(AF_INET , SOCK_DGRAM , IPPROTO_UDP);
    }

    void SetSocketTimeout(int socket, int usec, int sec) {
      struct timeval tv;
      tv.tv_sec = sec;
      tv.tv_usec = usec;
      if(setsockopt(socket, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0) {
        perror("Error");
      }
    }

    void CloseSocket(int socket) {
      close(socket);
    }

    aap::DNS::RR GetResourceRecord(std::string rr) {
      std::transform(rr.begin(), rr.end(), rr.begin(), ::toupper);
      aap::DNS::RR record = aap::DNS::RR::ALL;
      if(rr == "A") {
        record = aap::DNS::RR::A;
      } else if(rr == "AAAA") {
        record = aap::DNS::RR::AAAA;
      } else if(rr == "ALL") {
        record = aap::DNS::RR::ALL;
      } else {
        exit(1);
      }
      return record;
    }
  } // namespace DNS
} // namespace aap
