// Copyright 2018 vi1i.petal@gmail.com

#include <aap/Controller.hpp>
#include <aap/Model.hpp>

#include <unistd.h>

#include <iostream>
#include <regex>
#include <utility>
#include <memory>
#include <string>
#include <deque>

aap::Controller::Controller(std::shared_ptr<aap::Conf> conf) {
  this->mConf = conf;
  this->mIsFinished = false;
}

void aap::Controller::Run() {
  unsigned int MAX_THREADS = std::thread::hardware_concurrency();
  unsigned int THREAD_LIMIT = MAX_THREADS;
  std::map<size_t, std::shared_ptr<aap::Model>> threads;

  std::deque<std::shared_ptr<aap::Model>> models;

  if(this->mConf.get()->thread_count > MAX_THREADS) {
    THREAD_LIMIT = MAX_THREADS;
  }else{
    THREAD_LIMIT = this->mConf.get()->thread_count;
  }

  auto predicate = [&threads](std::thread& t) -> bool {
    std::hash<std::thread::id> hasher;
    if(threads.at(hasher(t.get_id()))->Finished() && t.joinable()) {
      auto it = threads.find(hasher(t.get_id()));
      threads.erase(it);
      t.join();
    }else{
      return false;
    }
    return true;
  };

  for(auto& elm : std::filesystem::recursive_directory_iterator(this->mConf.get()->zonefile_path, std::filesystem::directory_options::skip_permission_denied)) {

    std::string inFilename(elm.path().string());
    std::string outFilename(this->mConf.get()->save_path + "/" + elm.path().filename().string() + ".aap");

    std::shared_ptr<aap::Model> model(new aap::Model(inFilename, outFilename));
    model.get()->SetEDNS0Size(this->mConf.get()->edns0Size);
    model.get()->SetUseEDNS0(this->mConf.get()->useEdns0);
    model.get()->SetRR(this->mConf.get()->rr);
    model.get()->SetSocketTimeout(this->mConf.get()->socketTimeout);
    model.get()->SetDNSSEC(this->mConf.get()->dnssec);
    for(const auto& elm : this->mViews) {
      model.get()->Subscribe(aap::Model::Notification::STARTED, elm.get());
      model.get()->Subscribe(aap::Model::Notification::ANALYZING, elm.get());
      model.get()->Subscribe(aap::Model::Notification::LINE, elm.get());
      model.get()->Subscribe(aap::Model::Notification::FINISHED, elm.get());
      model.get()->Subscribe(aap::Model::Notification::DEBUG, elm.get());
    }
    models.push_back(model);
  }

  std::hash<std::thread::id> hasher;
  while(!models.empty()) {
    std::thread t;
    while(this->mThreads.size() >= THREAD_LIMIT) {
      sleep(1);
      this->mThreads.erase(std::remove_if(this->mThreads.begin(), this->mThreads.end(), predicate), this->mThreads.end());
    }
    t = std::thread(&aap::Model::Run, models.front());
    threads.insert(std::pair<size_t, std::shared_ptr<aap::Model>>(hasher(t.get_id()), models.front()));
    this->mThreads.push_back(std::move(t));
    models.pop_front();
  }

  std::cout << "Last thread_bucket_size legt" << std::endl;
  while(!this->mThreads.empty()) {
    std::cout << "Threads left: " << this->mThreads.size() << std::endl;
    sleep(1);
    this->mThreads.erase(std::remove_if(this->mThreads.begin(), this->mThreads.end(), predicate), this->mThreads.end());
  }
  this->mIsFinished = true;
}

bool aap::Controller::IsFinished() {
  return this->mIsFinished;
}

void aap::Controller::Add(std::shared_ptr<aap::View> view) {
  this->mViews.push_back(view);
}
