// Copyright 2018 vi1i.petal@gmail.com

#include <aap/Model.hpp>
#include <aap/DNS.hpp>

#include <sys/time.h>

#include <algorithm>
#include <fstream>
#include <istream>
#include <iostream>
#include <cstring>
#include <regex>
#include <string>
#include <chrono>
#include <ctime>
#include <filesystem>

aap::Model::Model(std::string inFilename, std::string outFilename) : mInFilename(inFilename), mOutFilename(outFilename), mQueryType(255), mEdns0Size(4096), mCType("ALL"), mSocketTimeout(500000), mFinished(false) {}

aap::Model::~Model() {
  aap::DNS::CloseSocket(this->mSocket);
  if(this->mFile.is_open()) {
    this->mFile.close();
  }
  this->Notify(Notification::FINISHED, this->mInFilename, "Cleaned Up");
}

void aap::Model::Run() {
  std::ios::sync_with_stdio(false);
  std::ifstream inFile(this->mInFilename, std::ios::in);
  this->SetOutFilename();
  this->mFile.open(this->mOutFilename);
  if(!inFile.is_open()) {
    exit(EXIT_FAILURE);
  }
  std::string base_filename = this->mInFilename.substr(this->mInFilename.find_last_of("/\\") + 1);
  std::regex re_tld("(\\w+?)_\\d\\d\\d");
  std::smatch matches;
  if (std::regex_search(base_filename, matches, re_tld)) {
    this->mTLD = matches[1].str();
    std::transform(this->mTLD.begin(), this->mTLD.end(),this->mTLD.begin(), ::toupper);
  } else {
    return;
  }

  this->Notify(Notification::STARTED, this->mInFilename, "");
  this->Notify(Notification::ANALYZING, this->mInFilename, "");

  size_t lineCnt = std::count(std::istreambuf_iterator<char>(inFile), std::istreambuf_iterator<char>(), '\n');
  inFile.unget();
  if(inFile.get() != '\n') {
    ++lineCnt;
  }
  size_t totalLines = lineCnt;
  inFile.clear();
  inFile.seekg(0);

  this->mSocket = aap::DNS::GetSocket();
  aap::DNS::SetSocketTimeout(this->mSocket, this->mSocketTimeout);

  std::string line;
  std::string printingLine;
  size_t curLine = 0;

  while(std::getline(inFile, line)) {
    if(curLine % ((lineCnt/100) + 1) == 0) {
      auto finish = std::chrono::high_resolution_clock::now();
      std::string percentage = std::to_string((curLine / (double)totalLines) * 100);
      this->Notify(Notification::LINE, this->mInFilename, percentage + "%");
      this->mFile << printingLine;
      this->mFile.flush();
      printingLine = "";
    }

    std::stringstream ss(line);
    std::string host("");
    std::string ns("");
    std::getline(ss, host, ' ');
    std::getline(ss, ns, ' ');
    std::getline(ss, ns, ' ');

    if(host.back() == '.') {
      host.pop_back();
    }else{
      host = host + "." + this->mTLD;
    }
    if(ns.back() == '.') {
      ns.pop_back();
    }else{
      ns = ns + "." + this->mTLD;
    }

    int query_type = this->mQueryType;
    int edns0_size = this->mEdns0Size;

    std::string ns_ip("");
    if(this->HasResolved(ns)) {
      ns_ip = this->GetResolved(ns);
    } else {
      char *c_ns = new char[ns.length() + 1];
      strcpy(c_ns, ns.c_str());

      unsigned char *uc_ns = (unsigned char*)&c_ns[0];
      char *c_query_ns = new char[std::string("8.8.8.8").length() + 1];
      std::strcpy(c_query_ns, std::string("8.8.8.8").c_str());

      ns_ip = aap::DNS::GetHostByName(this, this->mSocket, uc_ns, c_query_ns);

      delete [] c_query_ns;
      c_query_ns = NULL;

      delete [] c_ns;
      c_ns = NULL;

      this->SetResolved(ns, ns_ip);
    }

    if(ns_ip == "TEST" ||
        ns_ip == "ERROR:RCODE" ||
        ns_ip == "ERROR:NOFOUND" ||
        ns_ip == "ERROR:NOIPv4" ||
        ns_ip == "ERROR:2:SENDTO" ||
        ns_ip == "ERROR:2:RECVFROM" ||
        ns_ip == "ERROR:INVALIDIPv4" ) {
      curLine++;
      continue;
    }

    char *c_host = new char[host.length() + 1];
    std::strcpy(c_host, host.c_str());
    unsigned char *uc_host = (unsigned char*)c_host;

    char *c_ns_ip = new char[ns_ip.length() + 1];
    std::strcpy(c_ns_ip, ns_ip.c_str());

    std::string data = aap::DNS::GetHostByName(this, this->mSocket, uc_host, query_type, edns0_size, c_ns_ip, this->mUseEdns0, this->mDNSSEC);
    delete [] c_host;
    c_host = NULL;
    delete [] c_ns_ip;
    c_ns_ip = NULL;

    if(data == "ERROR:NOFOUND" ||
        data == "ERROR:1:SENDTO" ||
        data == "ERROR:1:RECVFROM") {
      curLine++;
      continue;
    }
    std::string uLine(host + "," + ns + "," + ns_ip + "," + data);
    printingLine += "\n" + uLine;
    
    curLine++;
  }
  this->mFile << printingLine;
  this->mFile.flush();

  this->Notify(Notification::LINE, this->mInFilename, "100%");
  inFile.close();
  this->mFile.close();
  this->Notify(Notification::FINISHED, this->mInFilename, "");
  this->mFinished = true;
}

void aap::Model::Subscribe(int notification, aap::IObserver* observer) {
  if(!this->mObservers.count(notification)) {
    this->mObservers[notification].push_back(observer);
    return;
  }
  for(const auto& o : this->mObservers[notification]) {
    if(observer == o) {
      return;
    }
  }
  this->mObservers[notification].push_back(observer);
}

void aap::Model::Unsubscribe(int notification, aap::IObserver* observer) {
  this->mObservers[notification].erase(
      std::remove(this->mObservers[notification].begin(),
        this->mObservers[notification].end(),
        observer));
}

void aap::Model::Notify(int notification, std::string filename, std::string message) {
  for(const auto& o : this->mObservers[notification]) {
    o->Update(notification, filename, message);
  }
}

void aap::Model::SetRR(aap::DNS::RR record) {
  this->mQueryType = record;
  this->mCType = aap::DNS::RR_to_string(record);
}

void aap::Model::SetUseEDNS0(int useEdns0) {
  this->mUseEdns0 = useEdns0;
}
void aap::Model::SetEDNS0Size(int edns0Size) {
  this->mEdns0Size = edns0Size;
}

void aap::Model::SetSocketTimeout(int socketTimeout) {
  this->mSocketTimeout = socketTimeout;
}

void aap::Model::SetDNSSEC(bool dnssec) {
  this->mDNSSEC = dnssec;
}

bool aap::Model::HasResolved(std::string nameserver) {
  return this->mResolved.find(nameserver) != this->mResolved.end();
}

void aap::Model::SetResolved(std::string nameserver, std::string ip) {
  this->mResolved[nameserver] = ip;
}

std::string aap::Model::GetResolved(std::string nameserver) {
  return this->mResolved.at(nameserver);
}

void aap::Model::SetOutFilename() {
  std::filesystem::path temp = this->mOutFilename;
  this->mOutFilename = temp.parent_path().string() + "/";

  this->mOutFilename += std::filesystem::path(this->mInFilename).stem().string();
  this->mOutFilename += "-" + this->mCType;
  this->mOutFilename += "-" + std::to_string(this->mSocketTimeout);
  this->mOutFilename += "-" + std::to_string(this->mUseEdns0);
  this->mOutFilename += "-" + std::to_string(this->mEdns0Size);
  this->mOutFilename += "-" + std::to_string(this->mDNSSEC);
  this->mOutFilename += ".aapo";
  this->Notify(Notification::DEBUG, this->mOutFilename, "");
}

auto aap::Model::Finished() -> bool {
  return this->mFinished;
}
