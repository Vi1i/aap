// Copyright 2018 vi1i.petal@gmail.com

#include <aap/View.hpp>
#include <aap/Model.hpp>    // Ties View to the model........

#include <unistd.h>

#include <thread>
#include <iostream>
#include <string>

void aap::View::Update(int notification, std::string filename, std::string message) {
    std::string noti;
    switch(notification) {
        case aap::Model::Notification::STARTED:
            noti = "STARTED";
            break;
        case aap::Model::Notification::ANALYZING:
            noti = "ANALYZING";
            break;
        case aap::Model::Notification::FINISHED:
            noti = "FINISHED";
            break;
        case aap::Model::Notification::LINE:
            noti = "LINE";
            break;
        case aap::Model::Notification::DEBUG:
            noti = "DEBUG";
            break;
        case aap::Model::Notification::ERROR:
            noti = "ERROR";
            break;
        default:
            noti = "UH OH";
            break;
    }
    static std::mutex mutex;
    std::lock_guard<std::mutex> lock(mutex);
    std::cout << noti << "::" << filename;
    if(message != "") {
        std::cout << "::" << message;
    }

    std::cout << std::endl;
}
