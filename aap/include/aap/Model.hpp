#ifndef MODEL_HPP
#define MODEL_HPP

#include <string>
#include <map>
#include <fstream>

#include <aap/ISubject.hpp>
#include <aap/IObserver.hpp>
#include <aap/Conf.hpp>
#include <aap/DNS.hpp>

namespace aap {
    class Model : public ISubject {
    public:
        enum Notification {
            STARTED = 0,
            ANALYZING,
            LINE,
            FINISHED,
            DEBUG,
            ERROR,
        };

        Model(std::string inFilename, std::string outFilename);

        virtual ~Model();

        void Run();
        void SetRR(aap::DNS::RR record);
        void SetRRString(std::string record);
        void SetEDNS0Size(int edns0Size);
        void SetUseEDNS0(int useEdns0);
        void SetSocketTimeout(int socketTimeout);
        void SetDNSSEC(bool dnssec);
        bool HasResolved(std::string nameserver);
        void SetResolved(std::string nameserver, std::string ip);
        std::string GetResolved(std::string nameserver);

        auto Finished() -> bool;

        virtual void Subscribe(int notification, aap::IObserver* observer);
        virtual void Unsubscribe(int notification, aap::IObserver* observer);
        virtual void Notify(int notification, std::string filename, std::string message);

    private:
        std::string mInFilename;
        std::string mOutFilename;
        int mQueryType;
        bool mUseEdns0;
        int mEdns0Size;
        std::string mCType;
        std::string mTLD;
        int mSocket;
        bool mDNSSEC;
        int mSocketTimeout;
        std::map<std::string, std::string> mResolved;
        std::ofstream mFile;

        bool mFinished;

        void SetOutFilename();
    };
}
#endif
