#ifndef ISUBJECT_HPP
#define ISUBJECT_HPP

#include <vector>
#include <map>

#include <aap/IObserver.hpp>

namespace aap {
    class ISubject {
    public:
        ISubject() {};
        virtual ~ISubject() {};
        virtual void Subscribe(int notification, aap::IObserver* observer) = 0;
        virtual void Unsubscribe(int notification, aap::IObserver* observer) = 0;
        virtual void Notify(int notification, std::string filename, std::string message) = 0;

    protected:
        typedef std::vector<aap::IObserver*> ObserverList;
        typedef std::map<int, ObserverList> ObserverMap;
        ObserverMap mObservers;
    };
}
#endif
