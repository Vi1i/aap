#ifndef VIEW_HPP
#define VIEW_HPP

#include <string>
#include <memory>
#include <mutex>
#include <signal.h>

#include <aap/Conf.hpp>
#include <aap/IObserver.hpp>

namespace aap {
    class View : public IObserver {
    public:
        View(std::shared_ptr<aap::Conf> conf) : mConf(move(conf)) {};
        ~View() = default;

        virtual void Update(int notification, std::string filename, std::string message);

    private:
        std::shared_ptr<aap::Conf> mConf;

        void Init();
    };
}
#endif
