#ifndef CONF_HPP
#define CONF_HPP

#include <string>
#include <memory>
#include <aap/DNS.hpp>

namespace aap {
    struct Conf {
        Conf() : thread_count(1), zonefile_path("~/zonefiles"), save_path("~/save"), edns0Size(4096), rr(aap::DNS::RR::ALL), socketTimeout(500000), dnssec(true), useEdns0(true) {}
        Conf(const aap::Conf &conf) = default;
        unsigned int thread_count;
        std::string zonefile_path;
        std::string save_path;
        bool useEdns0;
        int edns0Size;
        int socketTimeout;
        bool dnssec;
        aap::DNS::RR rr;
    };

    std::shared_ptr<aap::Conf> ReadConfigurationFile(const std::string & confPath);
}

#endif
