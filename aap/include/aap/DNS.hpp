#ifndef DNS_HPP
#define DNS_HPP

#include <string>

namespace aap {
  namespace DNS {
    enum RR {
      A = 1,
      NS = 2,
      MD = 3,
      MF = 4,
      CNAME = 5,
      SOA = 6,
      MB = 7,
      MG = 8,
      MR = 9,
      _NULL = 10,
      WKS = 11,
      PTR = 12,
      HINFO = 13,
      MINFO = 14,
      MX = 15,
      TXT = 16,
      RP = 17,
      FSDB = 18,
      SIG = 24,
      KEY = 25,
      AAAA = 28,
      LOC = 29,
      SRV = 33,
      NAPTR = 35,
      KX = 36,
      CERT = 37,
      DNAME = 39,
      APL = 42,
      DS = 43,
      SSHFP = 44,
      IPSECKEY = 45,
      RRSIG = 46,
      NSEC = 47,
      DNSKEY = 48,
      DHCID = 49,
      NSEC3 = 50,
      NSEC3PARAM = 51,
      TLSA = 52,
      HIP = 55,
      CDS = 59,
      CDNSKEY = 60,
      TKEY = 249,
      TSIG = 250,
      CAA = 257,
      TA = 32768,
      DLV = 32769,
      AXFR = 252,
      ALL = 255,
      IXFR = 251,
      OPT = 41,
    };

    static inline auto RR_to_string(RR rr) -> std::string {
      std::string ret("ALL");
      switch(rr) {
        case RR::A:
          ret = "A";
          break;
        case RR::NS:
          break;
        case RR::MD:
          break;
        case RR::MF:
          break;
        case RR::CNAME:
          break;
        case RR::SOA:
          break;
        case RR::MB:
          break;
        case RR::MG:
          break;
        case RR::MR:
          break;
        case RR::_NULL:
          break;
        case RR::WKS:
          break;
        case RR::PTR:
          break;
        case RR::HINFO:
          break;
        case RR::MINFO:
          break;
        case RR::MX:
          break;
        case RR::TXT:
          break;
        case RR::RP:
          break;
        case RR::FSDB:
          break;
        case RR::SIG:
          break;
        case RR::KEY:
          break;
        case RR::AAAA:
          ret = "AAAA";
          break;
        case RR::LOC:
          break;
        case RR::SRV:
          break;
        case RR::NAPTR:
          break;
        case RR::KX:
          break;
        case RR::CERT:
          break;
        case RR::DNAME:
          break;
        case RR::APL:
          break;
        case RR::DS:
          break;
        case RR::SSHFP:
          break;
        case RR::IPSECKEY:
          break;
        case RR::RRSIG:
          break;
        case RR::NSEC:
          break;
        case RR::DNSKEY:
          break;
        case RR::DHCID:
          break;
        case RR::NSEC3:
          break;
        case RR::NSEC3PARAM:
          break;
        case RR::TLSA:
          break;
        case RR::HIP:
          break;
        case RR::CDS:
          break;
        case RR::CDNSKEY:
          break;
        case RR::TKEY:
          break;
        case RR::TSIG:
          break;
        case RR::CAA:
          break;
        case RR::TA:
          break;
        case RR::DLV:
          break;
        case RR::AXFR:
          break;
        case RR::ALL:
          ret = "ALL";
          break;
        case RR::IXFR:
          break;
        case RR::OPT:
          break;
      }

      return ret;
    }

    struct DNS_HEADER {
      unsigned short id;          // ID ...
      unsigned char rd        :1; // Recursion desired if set
      unsigned char tc        :1; // Truncated if set
      unsigned char aa        :1; // Authoritive answer if set
      unsigned char opcode    :4; // Intent of message
      unsigned char qr        :1; // 0 = query/1 = response flag

      unsigned char rcode     :4; // Response code
      unsigned char cd        :1; // Checking disabled, DNSSEC
      unsigned char ad        :1; // Authenticated data, DNSSEC
      unsigned char z         :1; // It is reserved for future use
      unsigned char ra        :1; // Recursion available if set

      unsigned short q_count;     // Number of question entries
      unsigned short ans_count;   // Number of answer entries
      unsigned short auth_count;  // Number of authority entries
      unsigned short add_count;   // Number of resource entries
    };

#pragma pack(push, 1)
    struct OPT_RECORD {
      unsigned char domain_name;
      unsigned short type;
      unsigned short _class;
      unsigned int ttl;
      unsigned int rd_len;
    };
#pragma pack(pop)

    struct QUESTION {
      unsigned short qtype;
      unsigned short qclass;
    };
#pragma pack(push, 1)
    struct R_DATA {
      unsigned short type;
      unsigned short _class;
      unsigned int ttl;
      unsigned short data_len;
    };
#pragma pack(pop)
    struct RES_RECORD {
      unsigned char * name;
      struct R_DATA * resource;
      unsigned char * rdata;
    };

    struct QUERY {
      unsigned char *name;
      struct QUESTION *ques;
      struct OPT_RECORD *opt_rec;
    };

    aap::DNS::RR GetResourceRecord(std::string rr);

    void ReadName(unsigned char* reader, unsigned char* buffer, int* count);
    std::string hexStr(unsigned char *data, int len);
    std::string GetHostByName(void* context, int socket, unsigned char *host, char *ns_ip);
    std::string GetHostByName(void* context, int socket, unsigned char *host, int query_type, int edns0_size, char *ns_ip, bool use_edns0, bool dnssec);
    void ChangeToDnsNameFormat(unsigned char *dns, unsigned char *host);

    int GetSocket();
    void SetSocketTimeout(int socket, int usec, int sec = 0);
    void CloseSocket(int socket);

    void Notify(void* context, int notification, std::string filename, std::string message);
  }
}

#endif
