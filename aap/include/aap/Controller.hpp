#ifndef CONTROLLER_HPP
#define CONTROLLER_HPP

#include <string>
#include <vector>
#include <map>
#include <functional>
#include <memory>
#include <thread>
#include <filesystem>

#include <aap/Conf.hpp>
#include <aap/View.hpp>
#include <aap/Model.hpp>

namespace aap {
    class Controller {
    public:
        //Controller(std::shared_ptr<aap::Conf> conf, std::shared_ptr<aap::View> view);
        Controller(std::shared_ptr<aap::Conf> conf);
        ~Controller() = default;
        Controller(Controller &&) = default;

        void Run();
        void Add(std::shared_ptr<aap::View> view);
        bool IsFinished();

    private:
        bool mIsFinished;
        std::shared_ptr<aap::Conf> mConf;
        std::vector<std::thread> mThreads;
        std::vector<std::shared_ptr<aap::View>> mViews;
    };
}
#endif
