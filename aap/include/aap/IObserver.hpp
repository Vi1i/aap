#ifndef IOBSERVER_HPP
#define IOBSERVER_HPP

#include <string>

namespace aap {
    class IObserver {
    public:
        virtual ~IObserver() {}
        virtual void Update(int notification, std::string filename, std::string message) = 0;
    };
}
#endif
