#include <aap/zoneclean/Config.hpp>

#include <memory>
#include <cstdlib>
#include <zlib.h>
#include <iostream>
#include <vector>
#include <map>
#include <cstring>
#include <filesystem>
#include <chrono>
#include <regex>
#include <fstream>

std::ostream& operator<<(std::ostream& os, const aap::zoneclean::Config::data& datum) {
  return os << "{'inpath':'" << datum.inpath << "','outpath':'" << datum.outpath << "'}";
}

std::vector<char> readline(gzFile f) {
  std::vector<char> v(256);

  unsigned pos = 0;
  for(;;) {
    if(gzgets(f, &v[pos], v.size() - pos) == 0) {
      // end-of-file or error
      int err;
      const char *msg = gzerror(f, &err);
      if(err != Z_OK) {
        // handle error
      }
      break;
    }

    unsigned read = std::strlen(&v[pos]);
    if(v[pos + read - 1] == '\n') {
      if(pos + read >= 2 && v[pos + read - 2] == '\r') {
        pos = pos + read - 2;
      } else {
        pos = pos + read - 1;
      }
      break;
    }

    if(read == 0 || pos + read < v.size() - 1) {
      pos = read + pos;
      break;
    }

    pos = v.size() - 1;
    v.resize(v.size() * 2);
  }

  v.resize(pos);

  return v;
}

typedef struct {
  unsigned int lines_total;
  unsigned int lines_matched;
  unsigned int bytes_total;
  unsigned int bytes_matched;
  std::filesystem::path file;
} file_info;

auto clean(gzFile gzf, file_info fi, aap::zoneclean::Config::data datum) -> file_info {
  std::cout << "\tCleaning" << std::flush;
  //std::vector<std::string> lines;
  std::map<std::string, std::string> lines;
  std::regex type_basic("^(\\S+)\\s+(\\S+)\\s+(\\S+)\\s+(\\S+)\\s(\\S+)$");
  std::regex type_special("^(\\S+)\\s+(\\S+)\\s+(\\S+)$");
  std::smatch sm;
  unsigned int bytes_current = 0;
  unsigned int line_current = 0;
  unsigned int file_counter = 0;

  while(!gzeof(gzf)) {
    fi.lines_total++;

    // Get the line, convery it to a string for regex matching
    std::vector<char> cLine = readline(gzf);
    std::string sLine;
    for (std::vector<char>::const_iterator i = cLine.begin(); i != cLine.end(); ++i) {
      sLine += *i;
    }
    fi.bytes_total += sLine.size();

    // Check if the line matches our regex searches
    if(std::regex_match(sLine, sm, type_basic)) {
      if(sm[4] == "ns" || sm[4] == "NS") {
        sLine = sm[1].str() + " " + sm[4].str() + " " + sm[5].str();
        lines[sm[1].str()] = sLine;
        fi.lines_matched++;
        fi.bytes_matched += sLine.size();
        bytes_current += sLine.size();
      }
    } else if(std::regex_match(sLine, sm, type_special)) {
      if(sm[2] == "ns" || sm[2] == "NS") {
        sLine = sm[1].str() + " " + sm[2].str() + " " + sm[3].str();
        lines[sm[1].str()] = sLine;
        fi.lines_matched++;
        fi.bytes_matched += sLine.size();
        bytes_current += sLine.size();
      }
    }

    if(((double)bytes_current / (double) 1024 / (double) 1024) > 100) {
      std::cout << "." << std::flush;
      std::string counterStr;
      std::ofstream ofs;

      if(file_counter < 10) {
        counterStr = "00" + std::to_string(file_counter);
      } else if(file_counter < 100) {
        counterStr = "0" + std::to_string(file_counter);
      } else {
        counterStr = std::to_string(file_counter);
      }
      std::filesystem::path outfile(datum.outpath + "/" + fi.file.stem().stem().string() + "_" + counterStr + ".aap");

      ofs.open(outfile);
      for(const auto &line : lines) {
        ofs << line.second << std::endl;
      }
      ofs.close();

      lines.clear();
      bytes_current = 0;
      file_counter++;
    }
  }

  if(lines.size() > 0) {
    std::cout << "." << std::flush;
    std::string counterStr;
    std::ofstream ofs;

    if(file_counter < 10) {
      counterStr = "00" + std::to_string(file_counter);
    } else if(file_counter < 100) {
      counterStr = "0" + std::to_string(file_counter);
    } else {
      counterStr = std::to_string(file_counter);
    }
    std::filesystem::path outfile(datum.outpath + "/" + fi.file.stem().stem().string() + "_" + counterStr + ".aap");

    ofs.open(outfile);
    for(const auto &line : lines) {
      ofs << line.second << std::endl;
    }
    ofs.close();
  }
  std::cout << std::endl;

  gzrewind(gzf);
  return fi;
}

auto main(int argc, char * argv[]) -> int {
  std::shared_ptr<aap::zoneclean::Config> conf;
  if(argc == 2) {
    conf = std::make_shared<aap::zoneclean::Config>(argv[1]);
  } else {
    conf = std::make_shared<aap::zoneclean::Config>();
  }
  aap::zoneclean::Config::data datum = conf->GetConfigData();

  std::cout << datum << std::endl;
  std::vector<std::filesystem::path> files;
  for(const auto &entry : std::filesystem::directory_iterator(datum.inpath)) {
    if(entry.path().extension() == ".gz") {
      files.push_back(entry.path());
    }
  }

  if (!std::filesystem::is_directory(datum.outpath) || !std::filesystem::exists(datum.outpath)) {
    std::filesystem::create_directory(datum.outpath);
  }

  auto mainStart = std::chrono::system_clock::now();
  for(const auto &f : files) {
    std::cout << "Opening: " << f << std::endl;
    auto start = std::chrono::system_clock::now();
    gzFile gzf;
    gzf = gzopen (f.string().c_str(), "r");
    if (! gzf) {
      std::cerr << "gzopen of '" << f.string() << "' failed: " << strerror(errno);
    }

    file_info fi;
    fi.file = f;
    fi = clean(gzf, fi, datum);
    gzclose(gzf);

    unsigned int bytes = 0;

    auto end = std::chrono::system_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    std::cout << "\tTime Taken: " << elapsed.count() << "ms" << std::endl;
    std::cout << "\tDirty Size: " << (double) fi.bytes_total / (double) 1024 / (double) 1024 << "Mb" << std::endl;
    std::cout << "\tClean Size: " << (double) fi.bytes_matched / (double) 1024 / (double) 1024 << "Mb" << std::endl;
    std::cout << "\tLine Count: " << fi.lines_total << std::endl;
    std::cout << "\tLines Matched: " << fi.lines_matched << std::endl;
    std::cout << "\tPercentage Used: " << ((double) fi.lines_matched / (double) fi.lines_total) * 100 << "%" << std::endl;
    std::cout << "\tPercentage Reduced: " << ((double) fi.bytes_matched / (double) 1024 / (double) 1024) / ((double) fi.bytes_total / (double) 1024 / (double) 1024) * 100 << "%" << std::endl;

  }
  auto end = std::chrono::system_clock::now();
  auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - mainStart);
  std::cout << std::endl << "Total time: " << elapsed.count() << "ms" << std::endl;

  return EXIT_SUCCESS;
}
