#ifndef AAP_AAP_ZONECLEAN_HPP
#define AAP_AAP_ZONECLEAN_HPP

#include <string>
#include <ostream>

namespace aap { namespace zoneclean {
class Config {
public:
  typedef struct {
    std::string inpath;
    std::string outpath;
  } data;

  Config();
  Config(std::string filename);
  auto GetConfigData() -> aap::zoneclean::Config::data;

private:
  std::string filename_;
  aap::zoneclean::Config::data datum_;

  auto Init() -> void;
};
}}
#endif
